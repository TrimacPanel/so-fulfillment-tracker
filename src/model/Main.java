package model;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/view/mainscreen.fxml"));
        primaryStage.setTitle("SO Fulfillment Tracker");
        primaryStage.setScene(new Scene(root, 1500, 700));
        primaryStage.show();
    }

    public static void main(String[] args) {
        SOFulfillmentTracker sOFulfillmentTracker = new SOFulfillmentTracker();
        launch(args);
    }
}
