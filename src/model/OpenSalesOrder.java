package model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Date;

public class OpenSalesOrder {
    private int salesOrder;
    private String customer;
    private String purchaseOrder;
    private Date shippingDate;
    private ObservableList<LineItem> lineItems = FXCollections.observableArrayList();

    public OpenSalesOrder(int salesOrder, String customer, String purchaseOrder, Date shippingDate) {
        this.salesOrder = salesOrder;
        this.customer = customer;
        this.purchaseOrder = purchaseOrder;
        this.shippingDate = shippingDate;
    }

    public int getSalesOrder() {
        return salesOrder;
    }

    public void setSalesOrder(int salesOrder) {
        this.salesOrder = salesOrder;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(String purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public Date getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(Date shippingDate) {
        this.shippingDate = shippingDate;
    }

    public ObservableList<LineItem> getLineItems() {
        return lineItems;
    }

    public void addLineItems(ObservableList<LineItem> retrieveLineItemData) {
        this.lineItems = retrieveLineItemData;
    }
}
