package model;

import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.ArrayList;
import java.util.Properties;

public class TreeViewHelper {
    public ArrayList<TreeItem> getOpenSalesOrders() {
        ArrayList<TreeItem> treeItemsForTreeView = new ArrayList<TreeItem>();

        //Retrieving all sales order data to calculate % of completion
        //TODO: Combine both retrievals of sales order data
        ObservableList<OpenSalesOrder> openSalesOrders = SOFulfillmentTracker.getOpenSalesOrders();

        //Obtain a list of Open Sales Orders data, need customers, sales order number, sales person
        ArrayList<OpenSOTreeItem> openSalesOrdersDataForTreeView = retrieveSalesOrderDataForTreeView();

        //Obstain a list of distinct/unique customer names
        ArrayList<String> uniqueCustomerNames = retrieveDistinctCustomerData(); //Distinct == Unique

        double percentageOfCompletion = 0;

        for (String customerIterator : uniqueCustomerNames){
            //Add customer names to tree
            TreeItem newCustomer = new TreeItem(customerIterator);

            //Add sales orders to corresponding customer
            for (OpenSOTreeItem soIterator : openSalesOrdersDataForTreeView){
                double count = 0.0;
                double numOfLineItems = 0.0;

                for (OpenSalesOrder i : openSalesOrders){
                    if (Integer.parseInt(soIterator.getSalesOrder()) == i.getSalesOrder()){
                        ObservableList<LineItem> soLineItems = i.getLineItems();

                        for (LineItem x : soLineItems){
                            //Check if 2 conditions apply on each line item
                            if (x.getWorkOrderNum() != null || x.getQoh() >= 0){
                                //Count them as a hit
                                count++;
                            }
                        }

                        //Grab the number of line items on the sales order
                        if (numOfLineItems == 0.0){
                            numOfLineItems = soLineItems.size();
                        }
                    }
                }

                //Divide the total number of hit by the size of the line item array and you will get a percentage
                percentageOfCompletion = (int)((count / numOfLineItems)*100);

                if (soIterator.getCustomer().compareTo(customerIterator) == 0){
                    //TODO: Need to change the percentage of completion.
                    String display = percentageOfCompletion + "%      SO: " + soIterator.getSalesOrder() + "      Ship: " + soIterator.getShippingDate() + "      PO#: " + soIterator.getPurchaseOrder();
                    TreeItem newSO = new TreeItem(display);
                    newCustomer.getChildren().add(newSO);
                }
            }
            treeItemsForTreeView.add(newCustomer);
        }

        //TODO: Remove sales orders that are not for the current login salesman. Add the sales person number to parameter to this method.
        return treeItemsForTreeView;
    }

    private ArrayList<String> retrieveDistinctCustomerData() {
        /*config.properties looks like this:
        db.url=Your url
        db.dbName=Your database name
        db.user=Your username
        db.password=Your password*/
        String url = "";
        String dbName = "";
        String user = "";
        String password = "";

        Properties prop = new Properties();
        try (InputStream input = new FileInputStream("src/resources/macolaconfig.properties")){
            prop.load(input);
            url = prop.getProperty("db.url");
            dbName = prop.getProperty("db.dbName");
            user = prop.getProperty("db.user");
            password = prop.getProperty("db.password");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        String connectionUrl = "jdbc:sqlserver://" + url + ";databaseName=" + dbName + ";user=" + user + ";password=" + password;
        ResultSet rs = null;
        ArrayList<String> distinctCustomers = new ArrayList<>();

        try (Connection con = DriverManager.getConnection(connectionUrl); Statement stmt = con.createStatement();) {
            String SQL = "SELECT DISTINCT bill_to_name FROM [DATA_01].[dbo].[oeordhdr_sql] WHERE status <> 'L' AND ord_type <> 'C';";
            rs = stmt.executeQuery(SQL);

            while(rs.next()){
                distinctCustomers.add(rs.getString("bill_to_name"));
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }

        return distinctCustomers;
    }

    private ArrayList<OpenSOTreeItem> retrieveSalesOrderDataForTreeView() {
        String url = "";
        String dbName = "";
        String user = "";
        String password = "";

        Properties prop = new Properties();
        try (InputStream input = new FileInputStream("src/resources/macolaconfig.properties")){
            prop.load(input);
            url = prop.getProperty("db.url");
            dbName = prop.getProperty("db.dbName");
            user = prop.getProperty("db.user");
            password = prop.getProperty("db.password");
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        String connectionUrl = "jdbc:sqlserver://" + url + ";databaseName=" + dbName + ";user=" + user + ";password=" + password;
        ResultSet rs = null;
        ArrayList<OpenSOTreeItem> openSalesOrdersData = new ArrayList<>();

        try (Connection con = DriverManager.getConnection(connectionUrl); Statement stmt = con.createStatement();) {
            String SQL = "SELECT * FROM [DATA_01].[dbo].[oeordhdr_sql] WHERE status <> 'L' AND ord_type <> 'C';";
            rs = stmt.executeQuery(SQL);

            while(rs.next()){
                //Load Macola Data into Work Order object for processing
                String salesOrder = Integer.toString(rs.getInt("ord_no"));
                String customer = rs.getString("bill_to_name");
                int salesPerson = rs.getInt("slspsn_no");
                Date shippingDate = rs.getDate("shipping_dt");
                String purchaseOrder = rs.getString("oe_po_no");
                String percentageOfCompletion = "";
                openSalesOrdersData.add(new OpenSOTreeItem(salesOrder, customer, salesPerson, shippingDate, purchaseOrder, percentageOfCompletion));
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }

        return openSalesOrdersData;
    }


    private class OpenSOTreeItem {
        private String salesOrder;
        private String customer;
        private int salesPerson;
        private Date shippingDate;
        private String purchaseOrder;
        private String percentageOfCompletion;

        public OpenSOTreeItem(String salesOrder, String customer, int salesPerson, Date shippingDate, String purchaseOrder, String percentageOfCompletion) {
            this.salesOrder = salesOrder;
            this.customer = customer;
            this.salesPerson = salesPerson;
            this.shippingDate = shippingDate;
            this.purchaseOrder = purchaseOrder;
            this.percentageOfCompletion = percentageOfCompletion;
        }

        public String getSalesOrder() {
            return salesOrder;
        }

        public void setSalesOrder(String salesOrder) {
            this.salesOrder = salesOrder;
        }

        public String getCustomer() {
            return customer;
        }

        public int getSalesPerson() {
            return salesPerson;
        }

        public Date getShippingDate() {
            return shippingDate;
        }

        public void setShippingDate(Date shippingDate) {
            this.shippingDate = shippingDate;
        }

        public String getPurchaseOrder() {
            return purchaseOrder;
        }

        public void setPurchaseOrder(String purchaseOrder) {
            this.purchaseOrder = purchaseOrder;
        }

        public String getPercentageOfCompletion() {
            return percentageOfCompletion;
        }

        public void setPercentageOfCompletion(String percentageOfCompletion) {
            this.percentageOfCompletion = percentageOfCompletion;
        }
    }
}
