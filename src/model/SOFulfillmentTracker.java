package model;

import com.sun.javafx.util.Logging;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;
import java.util.logging.ErrorManager;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class SOFulfillmentTracker {
    private static ObservableList<OpenSalesOrder> openSalesOrders = FXCollections.observableArrayList();
    static final Logger logger = LogManager.getLogManager().getLogger(SOFulfillmentTracker.class.getName());

    SOFulfillmentTracker() {
        //TODO: Load only sales orders for specific salesman
        retrieveOpenSalesOrderData();
    }

    public static OpenSalesOrder getSalesOrderData(int selectedSalesOrderNumber) {
        for (OpenSalesOrder i : openSalesOrders) {
            if (i.getSalesOrder() == selectedSalesOrderNumber) {
                return i;
            }
        }
        return null;
    }

    public static void retrieveOpenSalesOrderData() {
        String connectionUrl = getSQLConnectionProperties("macolaconfig");
        String sqlQuery = "SELECT * FROM [DATA_01].[dbo].[oeordhdr_sql] WHERE status <> 'L' AND ord_type <> 'C';";

        try (Connection con = DriverManager.getConnection(connectionUrl);
             Statement stmt = con.createStatement();
             ResultSet rs = stmt.executeQuery(sqlQuery)) {
            while (rs.next()) {
                //Create OpenSalesOrder
                OpenSalesOrder newOpenSalesOrder = new OpenSalesOrder(rs.getInt("ord_no"),
                        trimString(rs.getString("bill_to_name")),
                        trimString(rs.getString("oe_po_no")),
                        rs.getDate("shipping_dt"));

                //Add Line Items to sales order
                newOpenSalesOrder.addLineItems(retrieveLineItemData(newOpenSalesOrder.getSalesOrder()));

                //Add to macola data collection to be returned
                openSalesOrders.add(newOpenSalesOrder);
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            logger.severe("Unable to access Open Sales Order data in Macola: " + e.toString());
        }
    }

    private static String getSQLConnectionProperties(String nameOfPropertiesFile) {
        String url = "";
        String dbName = "";
        String user = "";
        String password = "";

        Properties prop = new Properties();
        try (InputStream input = new FileInputStream("src/resources/" + nameOfPropertiesFile + ".properties")) {
            prop.load(input);
            url = prop.getProperty("db.url");
            dbName = prop.getProperty("db.dbName");
            user = prop.getProperty("db.user");
            password = prop.getProperty("db.password");
        } catch (IOException ex) {
            logger.severe("Unable to find config properties file");
        }

        return "jdbc:sqlserver://" + url + ";databaseName=" + dbName + ";user=" + user + ";password=" + password;
    }

    private static ObservableList<LineItem> retrieveLineItemData(int salesOrder) {
        ObservableList<LineItem> macolaLineItemData = FXCollections.observableArrayList();

        String connectionUrl = getSQLConnectionProperties("macolaconfig");
        ResultSet rs;

        try (Connection con = DriverManager.getConnection(connectionUrl); Statement stmt = con.createStatement()) {
            String SQL = "SELECT * FROM [DATA_01].[dbo].[vOpenSalesOrdersWithLineItems] WHERE ord_no = " + salesOrder;
            rs = stmt.executeQuery(SQL);

            while (rs.next()) {
                //Add to macola data collection to be returned
                //TODO: Need to add visibility to comment column
                LineItem newLineItem = new LineItem(rs.getInt("line_seq_no"), trimString(rs.getString("work_order_num")),
                        trimString(rs.getString("production_line")), rs.getDate("due_date"), rs.getDate("scheduled_date"), trimString(rs.getString("item_no")),
                        trimString(rs.getString("item_desc_1")), rs.getInt("tot_qty_ordered"),
                        rs.getInt("tot_qty_shipped"), rs.getInt("qty_on_hand"), rs.getInt("qty_allocated"));
                macolaLineItemData.add(newLineItem);
            }
        }
        // Handle any errors that may have occurred.
        catch (SQLException e) {
            e.printStackTrace();
        }
        return macolaLineItemData;
    }

    private static String trimString(String arg) {
        if (arg != null) {
            arg = arg.trim();
        }
        return arg;
    }

    public static ObservableList<OpenSalesOrder> getOpenSalesOrders() {
        return openSalesOrders;
    }
}
