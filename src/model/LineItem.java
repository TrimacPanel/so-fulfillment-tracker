package model;

import java.sql.Date;

public class LineItem {
    private int lineItemNum;
    private String workOrderNum;
    private String department;
    private Date workOrderDueDate;
    private Date schDate;
    private String itemSKU;
    private String itemDesc;
    private int qtyOrdered;
    private int qtyShipped;
    private int qoh;
    private int allocated;

    public LineItem(int lineItemNum, String workOrderNum, String department, Date workOrderDueDate, Date schDate, String itemSKU, String itemDesc, int qtyOrdered, int qtyShipped, int qoh, int allocated) {
        this.lineItemNum = lineItemNum;
        this.workOrderNum = workOrderNum;
        this.department = department;
        this.workOrderDueDate = workOrderDueDate;
        this.schDate = schDate;
        this.itemSKU = itemSKU;
        this.itemDesc = itemDesc;
        this.qtyOrdered = qtyOrdered;
        this.qtyShipped = qtyShipped;
        this.qoh = qoh;
        this.allocated = allocated;
    }

    public int getQtyShipped() {
        return qtyShipped;
    }

    public void setQtyShipped(int qtyShipped) {
        this.qtyShipped = qtyShipped;
    }

    public Date getWorkOrderDueDate() {
        return workOrderDueDate;
    }

    public void setWorkOrderDueDate(Date workOrderDueDate) {
        this.workOrderDueDate = workOrderDueDate;
    }

    public int getLineItemNum() {
        return lineItemNum;
    }

    public void setLineItemNum(int lineItemNum) {
        this.lineItemNum = lineItemNum;
    }

    public String getWorkOrderNum() {
        return workOrderNum;
    }

    public void setWorkOrderNum(String workOrderNum) {
        this.workOrderNum = workOrderNum;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public Date getSchDate() {
        return schDate;
    }

    public void setSchDate(Date schDate) {
        this.schDate = schDate;
    }

    public String getItemSKU() {
        return itemSKU;
    }

    public void setItemSKU(String itemSKU) {
        this.itemSKU = itemSKU;
    }

    public String getItemDesc() {
        return itemDesc;
    }

    public void setItemDesc(String itemDesc) {
        this.itemDesc = itemDesc;
    }

    public int getQtyOrdered() {
        return qtyOrdered;
    }

    public void setQtyOrdered(int qtyOrdered) {
        this.qtyOrdered = qtyOrdered;
    }

    public int getQoh() {
        return qoh;
    }

    public void setQoh(int qoh) {
        this.qoh = qoh;
    }

    public int getAllocated() {
        return allocated;
    }

    public void setAllocated(int allocated) {
        this.allocated = allocated;
    }
}
