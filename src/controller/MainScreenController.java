package controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.text.Text;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import model.LineItem;
import model.OpenSalesOrder;
import model.SOFulfillmentTracker;
import model.TreeViewHelper;

import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainScreenController implements Initializable {
    TreeItem rootItem = new TreeItem("Open Sales Orders");

    @FXML
    private Label txtTextUserID;

    @FXML
    private TreeView<String> treeViewID;

    @FXML
    private Label txtSalesOrderID;

    @FXML
    private Label txtCustomerID;

    @FXML
    private Label txtPONumID;

    @FXML
    private Label txtDueDateID;

    @FXML
    private TableView<LineItem> salesOrderTableView;

    @FXML
    private TableColumn<LineItem, Integer> colLineID;

    @FXML
    private TableColumn<LineItem, Integer> colWorkOrderID;

    @FXML
    private TableColumn<LineItem, String> colDepartmentID;

    @FXML
    private TableColumn<LineItem, Date> colSchDateID;

    @FXML
    private TableColumn<LineItem, Date> colDueDateID;

    @FXML
    private TableColumn<LineItem, String> colSKUID;

    @FXML
    private TableColumn<LineItem, String> colDescID;

    @FXML
    private TableColumn<LineItem, Integer> colQtyOrdID;

    @FXML
    private TableColumn<LineItem, Integer> colQtyShippedID;

    @FXML
    private TableColumn<LineItem, Integer> colQOHID;

    @FXML
    private TableColumn<LineItem, Integer> colAllocatedID;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        //Populate TreeView
        TreeViewHelper helper = new TreeViewHelper();
        ArrayList<TreeItem> openSalesOrders = helper.getOpenSalesOrders();
        rootItem.getChildren().addAll(openSalesOrders);
        treeViewID.setRoot(rootItem);
        treeViewID.getSelectionModel().selectFirst();
        expandTreeView(rootItem);
        treeViewID.setShowRoot(false);

        //Populate columns
        colLineID.setCellValueFactory(new PropertyValueFactory<LineItem, Integer>("lineItemNum"));
        colWorkOrderID.setCellValueFactory(new PropertyValueFactory<LineItem, Integer>("workOrderNum"));
        colDepartmentID.setCellValueFactory(new PropertyValueFactory<LineItem, String>("department"));
        colSchDateID.setCellValueFactory(new PropertyValueFactory<LineItem, Date>("schDate"));
        colDueDateID.setCellValueFactory(new PropertyValueFactory<LineItem, Date>("workOrderDueDate"));
        colSKUID.setCellValueFactory(new PropertyValueFactory<LineItem, String>("itemSKU"));
        colDescID.setCellValueFactory(new PropertyValueFactory<LineItem, String>("itemDesc"));
        colQtyOrdID.setCellValueFactory(new PropertyValueFactory<LineItem, Integer>("qtyOrdered"));
        colQtyShippedID.setCellValueFactory(new PropertyValueFactory<LineItem, Integer>("qtyShipped"));
        colQOHID.setCellValueFactory(new PropertyValueFactory<LineItem, Integer>("qoh"));
        colAllocatedID.setCellValueFactory(new PropertyValueFactory<LineItem, Integer>("allocated"));
    }

    @FXML
    void onActionFilterButton(ActionEvent event) {
        //TODO: Filter Button
    }

    @FXML
    void onActionPrintButton(ActionEvent event) {
        //TODO: Print Button
    }

    @FXML
    void onActionRefreshButton(ActionEvent event) {
        SOFulfillmentTracker.retrieveOpenSalesOrderData();

        //Populate TreeView
        TreeViewHelper helper = new TreeViewHelper();
        ArrayList<TreeItem> openSalesOrders = helper.getOpenSalesOrders();
        rootItem.getChildren().addAll(openSalesOrders);
        treeViewID.setRoot(rootItem);
        treeViewID.getSelectionModel().selectFirst();
        expandTreeView(rootItem);
        treeViewID.setShowRoot(false);

        TreeItem selectedItem = treeViewID.getSelectionModel().getSelectedItem();
        if (selectedItem != null){
            populateSalesOrderView(selectedItem);
        }
    }

    @FXML
    void onActionSearchButton(ActionEvent event) {
        //TODO: Currently works, but really should disable the Ok Button for incorrect text input
        Stage primaryStage = (Stage) ((Button) event.getSource()).getScene().getWindow();
        TextInputDialog dialog = new TextInputDialog("");
        dialog.setTitle("");
        dialog.setHeaderText("Sales Order Search");
        dialog.setContentText("Please enter your sales order number:");
        dialog.initStyle(StageStyle.UTILITY);
        dialog.initOwner(primaryStage);
        dialog.initModality(Modality.WINDOW_MODAL);

        // Traditional way to get the response value.
        Optional<String> result = dialog.showAndWait();
        if (result.isPresent() && isNumeric(result.get())) {
            TreeItem searchResult = treeViewSearch(rootItem, result.get());

            treeViewID.getSelectionModel().select(searchResult);
            treeViewID.scrollTo(treeViewID.getRow(searchResult));
            TreeItem selectedItem = treeViewID.getSelectionModel().getSelectedItem();
            if (selectedItem != null) {
                populateSalesOrderView(selectedItem);
            }
        }
    }

    private void expandTreeView(TreeItem<?> item){
        if(item != null && !item.isLeaf()){
            item.setExpanded(true);
            for(TreeItem<?> child:item.getChildren()){
                expandTreeView(child);
            }
        }
    }

    private TreeItem<String> treeViewSearch(TreeItem<String> rootItem, String text) {
        if (rootItem != null && rootItem.getValue().equals(text))
            return rootItem;

        for (TreeItem<String> child : rootItem.getChildren()){
            TreeItem<String> s = treeViewSearch(child, text);
            if(s!=null)
                return s;

        }
        return null;
    }

    @FXML
    void onActionSettingsButton(ActionEvent event) {
        //TODO: Settings menu
    }

    @FXML
    void onMouseClickedTreeView(MouseEvent event) {
        TreeItem selectedItem = treeViewID.getSelectionModel().getSelectedItem();
        if (selectedItem != null){
            populateSalesOrderView(selectedItem);
        }

    }

    private void populateSalesOrderView(TreeItem selectedItem) {

        int selectedSalesOrderNumber = 0;

        if (selectedItem.isLeaf()) {
            String[] result = ((String)selectedItem.getValue()).split("SO:\\s|Ship:");
            selectedSalesOrderNumber = Integer.parseInt(result[1].trim());
        }

        if (selectedSalesOrderNumber != 0) {
            OpenSalesOrder selectedSalesOrderObject = SOFulfillmentTracker.getSalesOrderData(selectedSalesOrderNumber);
            if (selectedSalesOrderObject != null) {
                txtSalesOrderID.setText(Integer.toString(selectedSalesOrderObject.getSalesOrder()));
                txtCustomerID.setText(trimString(selectedSalesOrderObject.getCustomer()));
                txtPONumID.setText(trimString(selectedSalesOrderObject.getPurchaseOrder()));
                txtDueDateID.setText(selectedSalesOrderObject.getShippingDate().toString());

                salesOrderTableView.getItems().setAll(selectedSalesOrderObject.getLineItems());
                autoResizeColumns(salesOrderTableView);
            } else {
                //TODO: Error: MacolaData didn't return that Sales Order. Contact IT.
            }
        }
    }

    /*
    Taken from https://stackoverflow.com/questions/14650787/javafx-column-in-tableview-auto-fit-size
    Auto resizes the columns in the TableView by looking at the width of the data
     */
    public static void autoResizeColumns(TableView<?> table) {
        //Set the right policy
        table.setColumnResizePolicy(TableView.UNCONSTRAINED_RESIZE_POLICY);
        table.getColumns().forEach(column ->
        {
            //Minimal width = column header
            Text t = new Text(column.getText());
            double max = t.getLayoutBounds().getWidth();
            for (int i = 0; i < table.getItems().size(); i++) {
                //cell must not be empty
                if (column.getCellData(i) != null) {
                    t = new Text(column.getCellData(i).toString());
                    double calcWidth = t.getLayoutBounds().getWidth();
                    //remember new max-width
                    if (calcWidth > max) {
                        max = calcWidth;
                    }
                }
            }
            //set the new max-width with some extra space
            column.setPrefWidth(max + 10.0d);
        });
    }

    private static String trimString(String arg) {
        if (arg != null) {
            arg = arg.trim();
        }
        return arg;
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
